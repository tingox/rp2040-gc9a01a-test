//! Draw a square, circle and triangle on the screen using the embedded_graphics library
//! over a 4 wire SPI interface.
//!
//! This is an example for the rp2040, more specifically the Raspberry Pi Pico.
//!
//! Wiring connections are as follows:
//!
//! ```
//! 3V3    -> VCC
//! GND    -> GND
//! GPIO8  -> D/C
//! GPIO9  -> CS
//! GPIO10 -> CLK
//! GPIO11 -> DIN
//! GPIO12 -> RST
//! GPIO13 -> BL
//! ```
//!
//! These default settings should also work when using the [RoundyPi](https://github.com/sbcshop/RoundyPi).
//! If you have a waveshare *RP2040 MCU Board*, you should use gpio 25 for backlight control (`BL`).

#![no_std]
#![no_main]

use panic_halt as _;
use rp_pico as bsp;

use bsp::entry;
use fugit::RateExtU32;

use display_interface_spi::SPIInterface;
use embedded_graphics::prelude::*;
use embedded_graphics::{
    pixelcolor::Rgb565,
    primitives::{Circle, PrimitiveStyleBuilder, Rectangle, Triangle, Line},
};

use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    gpio, pac, pwm,
    sio::Sio,
    spi,
    watchdog::Watchdog,
};

use defmt::*;
use defmt_rtt as _;

#[entry]
fn main() -> ! {
    info!("Started!");
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    // External high-speed crystal on the pico board is 12Mhz
    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // These are implicitly used by the spi driver if they are in the correct mode
    let _spi_sclk = pins.gpio10.into_mode::<gpio::FunctionSpi>();
    let _spi_mosi = pins.gpio11.into_mode::<gpio::FunctionSpi>();
    let spi_cs = pins.gpio9.into_push_pull_output();

    // Create an SPI driver instance for the SPI1 device
    let spi = spi::Spi::<_, _, 8>::new(pac.SPI1);

    // Exchange the uninitialised SPI driver for an initialised one
    let spi = spi.init(
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
        8_000_000u32.Hz(),
        &embedded_hal::spi::MODE_0,
    );

    let dc_pin = pins.gpio8.into_push_pull_output();
    let rst_pin = pins.gpio12.into_push_pull_output();

    let spi_interface = SPIInterface::new(spi, dc_pin, spi_cs);

    // initialize PWM for backlight
    let pwm_slices = pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // Configure PWM6
    let mut pwm = pwm_slices.pwm6;
    pwm.set_ph_correct();
    pwm.enable();

    // Output channel B on PWM6 to GPIO 13
    let mut channel = pwm.channel_b;
    channel.output_to(pins.gpio13);

    // Create display driver
    let mut display = gc9a01a::GC9A01A::new(spi_interface, rst_pin, channel);
    // Bring out of reset
    display.reset(&mut delay).unwrap();
    // Turn on backlight
    display.set_backlight(55000);
    // Initialize registers
    display.initialize(&mut delay).unwrap();
    // Fill screen with single color
    // original: CSS_FOREST_GREEN
    // colors: CSS_BLUE, CSS_CADET_BLUE, CSS_BEIGE, CSS_BISQUE, CSS_BLANCHED_ALMOND, CSS_BROWN, CSS_BURLY_WOOD, CSS_CHOCOLATE, CSS_CORAL,
    // CSS_CORNFLOWER_BLUE, CSS_CRIMSON, CSS_CYAN, CSS_DARK_CYAN, CSS_DARK_GOLDENROD, CSS_DARK_GRAY, CSS_DARK_KHAKI, CSS_DARK_MAGENTA,
    // CSS_DARK_OLIVE_GREEN, CSS_DARK_ORANGE, CSS_DARK_ORCHID, CSS_DARK_SALMON, CSS_DARK_SEA_GREEN, CSS_DARK_SLATE_BLUE, 
    // CSS_DARK_TURQUOISE, CSS_DARK_VIOLET, CSS_DEEP_PINK, CSS_DEEP_SKY_BLUE, CSS_DIM_GRAY, CSS_DODGER_BLUE, CSS_FLORAL_WHITE, 
    // CSS_FOREST_GREEN, CSS_FUCHSIA, CSS_GAINSBORO, CSS_GHOST_WHITE, CSS_GOLD, CSS_GOLDENROD, CSS_GRAY, CSS_GREEN_YELLOW, 
    // CSS_HONEYDEW, CSS_HOT_PINK, CSS_INDIAN_RED, CSS_INDIGO, CSS_IVORY, CSS_KHAKI, CSS_LAVENDER, CSS_LAVENDER_BLUSH, CSS_LAWN_GREEN,
    // CSS_LEMON_CHIFFON, CSS_LIGHT_BLUE, CSS_LIGHT_CORAL, CSS_LIGHT_CYAN, CSS_LIGHT_GOLDENROD_YELLOW, CSS_LIGHT_GRAY,
    // CSS_LIGHT_GREEN, CSS_LIGHT_PINK, CSS_LIGHT_SALMON, CSS_LIGHT_SEA_GREEN, CSS_LIGHT_SKY_BLUE, CSS_LIGHT_SLATE_GRAY, CSS_LIGHT_STEEL_BLUE,
    // CSS_LIGHT_YELLOW, CSS_LIME, CSS_LIME_GREEN, CSS_LINEN, CSS_MAGENTA, CSS_MEDIUM_AQUAMARINE, CSS_MEDIUM_ORCHID, CSS_MEDIUM_PURPLE,
    // CSS_MEDIUM_SEA_GREEN, CSS_MEDIUM_SLATE_BLUE, CSS_MEDIUM_TURQUOISE, CSS_MEDIUM_VIOLET_RED, CSS_MINT_CREAM, CSS_MISTY_ROSE,
    // CSS_MOCCASIN, CSS_NAVAJO_WHITE, CSS_OLD_LACE, CSS_OLIVE, CSS_OLIVE_DRAB, CSS_ORANGE, CSS_ORANGE_RED, CSS_ORCHID, CSS_PALE_GOLDENROD, 
    //  CSS_PALE_GREEN, CSS_PALE_TURQUOISE, CSS_PALE_VIOLET_RED, CSS_PAPAYA_WHIP, CSS_PEACH_PUFF, CSS_PERU, CSS_PINK, CSS_PLUM, CSS_POWDER_BLUE,
    // CSS_PURPLE, CSS_REBECCAPURPLE, CSS_ROSY_BROWN, CSS_ROYAL_BLUE, CSS_SADDLE_BROWN, CSS_SALMON, CSS_SANDY_BROWN, CSS_SEA_GREEN,
    // CSS_SEASHELL, CSS_SIENNA, CSS_SILVER, CSS_SKY_BLUE, CSS_SLATE_BLUE, CSS_SLATE_GRAY, CSS_SNOW, CSS_SPRING_GREEN, CSS_STEEL_BLUE, CSS_TAN,
    // CSS_TEAL, CSS_THISTLE, CSS_TOMATO, CSS_TURQUOISE, CSS_VIOLET, CSS_WHEAT, CSS_WHITE_SMOKE, CSS_YELLOW, CSS_YELLOW_GREEN, 
    // GREEN, BLUE, YELLOW, MAGENTA, CYAN, 
    //
    // good colors: CSS_BLACK, CSS_DARK_BLUE, CSS_DARK_GREEN, CSS_DARK_RED, CSS_DARK_SLATE_GRAY, CSS_FIRE_BRICK, CSS_GREEN, CSS_MAROON,
    // CSS_MEDIUM_BLUE, CSS_MEDIUM_SPRING_GREEN, CSS_MIDNIGHT_BLUE, CSS_NAVY, CSS_RED, CSS_WHITE, 
    // BLACK, RED, WHITE,
    //
    //display.clear(Rgb565::WHITE).unwrap();
    display.clear(Rgb565::BLACK).unwrap();

    let yoffset = 100;
    let coffset = 120;

    let style_red = PrimitiveStyleBuilder::new()
        .stroke_width(2)
        .stroke_color(Rgb565::CSS_RED) // CSS_RED
        .build();

    let style_green = PrimitiveStyleBuilder::new()
        .stroke_width(2)
        .stroke_color(Rgb565::CSS_GREEN) // CSS_GREEN
        .build();

    let style_blue = PrimitiveStyleBuilder::new()
        .stroke_width(2)
        .stroke_color(Rgb565::CSS_BLUE) // CSS_BLUE
        .build();

    // screen outline for the round 1.28 inch Waveshare display
    // point 1, 1 is center?
    // Circle - the Point is the upper left corner
    Circle::new(Point::new(0, 0), 240)
        .into_styled(style_blue)
        .draw(&mut display)
        .unwrap();
    // center of display is 240 / 2 = 120
    // top left of circle is diameter / 2
    Circle::new(Point::new(coffset - 200/2, coffset - 200/2), 200)
        .into_styled(style_red)
        .draw(&mut display)
        .unwrap();

    // triangle
    Triangle::new(
        Point::new(50, 32 + yoffset),
        Point::new(50 + 32, 32 + yoffset),
        Point::new(50 + 8, yoffset),
    )
    .into_styled(style_green)
    .draw(&mut display)
    .unwrap();

    // square
    // start Point is upper left corner
    Rectangle::new(Point::new(coffset - (32 / 2), coffset - (32 / 2)), Size::new_equal(32))
        .into_styled(style_red)
        .draw(&mut display)
        .unwrap();

    // circle
    Circle::new(Point::new(170, yoffset), 32)
        .into_styled(style_blue)
        .draw(&mut display)
        .unwrap();
        
    Line::new(Point::new(1,coffset), Point::new(241,coffset))
        .into_styled(style_green)
        .draw(&mut display)
        .unwrap();
        
    Line::new(Point::new(coffset,1), Point::new(coffset,245))
        .into_styled(style_red)
        .draw(&mut display)
        .unwrap();

    info!("all done!");

    exit()
}

pub fn exit() -> ! {
    loop {
        cortex_m::asm::bkpt();
    }
}
