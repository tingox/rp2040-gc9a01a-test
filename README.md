# rp2040-gc9a01a-test

Example for driving a small OLED screen with the [Rust GC9A01A driver](https://gitlab.com/jspngh/gc9a01a-rs) based on the examples there.

The display is a 1.28 inch round OLED with 240 x 240 pixels resolution.

If I try to draw a circle with origin in Point(1,1) with diameter 239, it looks ok, If I try to draw another circle with origin in Point(1,1) and 
diameter 230, it looks like it is offset (it touches the outer circle on one side). Strange.
I also tried origin (0,0). No change. Stranger.

Ok, read the documentation! For embedded-graphics, the Point(x,y) for Circles, Rectnagles, etc. are upper left corner, not center.
